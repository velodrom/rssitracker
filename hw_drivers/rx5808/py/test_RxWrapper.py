import unittest
from RxWrapper import RxWrapper

class TestRxWrapper(unittest.TestCase):
    def test_measure_rssi(self):
    	gpioChPins = [16,20,21]
        rx5808 = RxWrapper()
        rx5808.Configure(gpioChPins)
        rssi = rx5808.MeasureRssi()

        print('test_measure_rssi: ' + str(rssi))
        self.assertTrue(rssi != 0)

if __name__ == "__main__":
    unittest.main()
