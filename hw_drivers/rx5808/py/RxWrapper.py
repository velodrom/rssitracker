#from ctypes import cdll
from ctypes import *
from array import array

rxlib = cdll.LoadLibrary('../hw_drivers/libs/librx.so')
numChBits = 3

class RxChGpioPins(Structure):
    _fields_ = [("chPins", c_byte*3)]

class RxWrapper(object):
    def __init__(self):
        self.adc = rxlib.Mcp3204_new()
        self.rx  = rxlib.Rx5808_new(self.adc)

    def Configure(self, gpioChPins):
        if (len(gpioChPins) == 3):
            rxConfigFunc = rxlib.Rx5808_config
            chPins       = bytearray(gpioChPins)
            gpioCfg      = RxChGpioPins((c_byte*3)(*list(chPins)))
            rxConfigFunc(self.rx, gpioCfg)

    def MeasureRssi(self):
        chBitmask = 0
        rssi = rxlib.RunOneMeasurement(chBitmask)
        return rssi
