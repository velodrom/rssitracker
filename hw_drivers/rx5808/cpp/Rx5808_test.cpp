#include <gtest/gtest.h>
#include "Rx5808.hpp"
#include "Mcp3204.hpp"

class Mcp3204Mock : public Mcp3204
{
public:
    Mcp3204Mock(): numMeas(0){};
    bool     IsConfigured() {return true;}
    uint32_t Measure()
    {
        numMeas++;
        return numMeas;
    }
private:
    uint32_t numMeas;
};

TEST(Rx5808Test, CreateClass)
{
    Mcp3204Mock dummyIf;
    Rx5808 rx(&dummyIf);
    EXPECT_TRUE(&rx != NULL);
}

TEST(Rx5808Test, ConfigureGpio)
{
    Mcp3204Mock dummyIf;
    Rx5808 rx(&dummyIf);
    const uint8_t chPins[3] = {16,20,21};
    rx.Configure(chPins);
    EXPECT_TRUE(rx.IsConfigured());
}

TEST(Rx5808Test, GetOneMeasurement)
{
    Mcp3204Mock dummyIf;
    Rx5808 rx(&dummyIf);
    const uint8_t chPins[3] = {16,20,21};
    rx.Configure(chPins);

    MeasParams params = {.rssiParams ={
            chBitmask : 0x0,
            measPeriod : 1
    }};
    MeasData *data;
    data = rx.Measure(params);

    EXPECT_EQ(  1, data->rssiMeasData.numRssiMeas);
    EXPECT_EQ(0x1, data->rssiMeasData.rssiMeas[0]);
}

TEST(Rx5808Test, ReturnNullIfMeasPeriodNotFull)
{
    Mcp3204Mock dummyIf;
    Rx5808 rx(&dummyIf);
    const uint8_t chPins[3] = {16,20,21};
    rx.Configure(chPins);

    MeasParams params = {.rssiParams ={
        chBitmask : 0x0,
        measPeriod : 2
    }};

    MeasData *data;
    data = rx.Measure(params);

    EXPECT_EQ(NULL, data);
}

TEST(Rx5808Test, ValidDataAfterLongMeasPeriod)
{
    Mcp3204Mock dummyIf;
    Rx5808 rx(&dummyIf);
    const uint8_t chPins[3] = {16,20,21};
    rx.Configure(chPins);

    MeasParams params = {.rssiParams ={
        chBitmask : 0x0,
        measPeriod : 2
    }};

    MeasData *data;
    data = rx.Measure(params);
    EXPECT_TRUE(data == NULL);
    data = rx.Measure(params);

    EXPECT_TRUE(data != NULL);
    EXPECT_EQ(2, data->rssiMeasData.numRssiMeas);
    EXPECT_EQ(1, data->rssiMeasData.rssiMeas[0]);
    EXPECT_EQ(2, data->rssiMeasData.rssiMeas[1]);
}

TEST(Rx5808Test, ValidDataOnSecondPeriod)
{
    Mcp3204Mock dummyIf;
    Rx5808 rx(&dummyIf);
    const uint8_t chPins[3] = {16,20,21};
    rx.Configure(chPins);

    MeasParams params = {.rssiParams ={
        chBitmask : 0x0,
        measPeriod : 1
    }};

    MeasData *data;
    data = rx.Measure(params);

    EXPECT_TRUE(data != NULL);
    EXPECT_EQ(1, data->rssiMeasData.numRssiMeas);
    EXPECT_EQ(1, data->rssiMeasData.rssiMeas[0]);

    data = rx.Measure(params);
    EXPECT_TRUE(data != NULL);
    EXPECT_EQ(1, data->rssiMeasData.numRssiMeas);
    EXPECT_EQ(2, data->rssiMeasData.rssiMeas[0]);
}
