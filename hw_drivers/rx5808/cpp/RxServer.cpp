#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <arpa/inet.h>

class RxServer
{
public:
	RxServer() :
		port(16003)
	{
	}

	void StartServer()
	{
		struct addrinfo hints, *res;
		int listener;

		memset(&hints, 0, sizeof(hints));
		hints.ai_family   = AF_UNSPEC; // IPV4 & IPV6 are ok
		hints.ai_socktype = SOCK_STREAM; // TCP
		hints.ai_flags    = AI_PASSIVE;

		char portChar[6];
		sprintf(portChar, "%d", port);
		int status = getaddrinfo(NULL, portChar, &hints, &res);
		if (status != 0)
			printf("Failed to get address info\n");

		listener = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
		if (listener < 0)
			printf("Failed to create listener\n");

		status = bind(listener, res->ai_addr, res->ai_addrlen);
		if (status != 0)
			printf("Failed to bind socket\n");

		status = listen(listener, 10);
		if (status != 0)
			printf("Failed to start listening for new connections\n");

		freeaddrinfo(res);

		struct sockaddr_storage client_addr;
		socklen_t addr_size = sizeof(client_addr);
		char ipvAddr[INET6_ADDRSTRLEN];

		printf("Accepting connections\n");
		while(1)
		{
			socketFd = accept(listener, (struct sockaddr*) &client_addr, &addr_size);
			if (socketFd < 0)
			{
				printf("Problem with acceping connection\n");
				continue;
			}
			else
			{
				printf(".");
			}

			inet_ntop(client_addr.ss_family,
				      GetInAddr((struct sockaddr*) &client_addr),
				      ipvAddr,
				      sizeof(ipvAddr));
			printf("Connected to %s\n", ipvAddr);
			char welcomeMsg[] = "Welcome";
			status = send(socketFd, welcomeMsg, sizeof(welcomeMsg), 0);
			if (status == -1)
			{
				close(socketFd);
				continue;
			}
		}

		close(socketFd);
	}

	void * GetInAddr(struct sockaddr *addr)
	{
		if (addr->sa_family == AF_INET)
			return &(((struct sockaddr_in*)addr)->sin_addr);
		else
			return &(((struct sockaddr_in6*)addr)->sin6_addr);
	}

private:
	int socketFd;
	int port;
};

// Todo, catch SIGINT interrupt and close server
int main(int argc, char *argv[])
{
	RxServer *rxSrv = new RxServer();
	rxSrv->StartServer();

	return 0;
}