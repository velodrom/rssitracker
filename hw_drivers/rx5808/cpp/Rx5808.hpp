#ifndef RX5808_HPP
#define RX5808_HPP

#include <stdint.h>
#include "AdcIf.hpp"
#include "MeasTypes.hpp"
#include "PhyMeasIf.hpp"

extern "C"
{

const uint16_t numChannelBits         = 3;

class Rx5808 : public PhyMeasIf
{
public:
    Rx5808(AdcIf *newAdcIf);
    ~Rx5808();

    MeasData *Measure(const MeasParams &params);
    void      Configure(const uint8_t chPins[numChannelBits]);
    bool      IsConfigured();
    void      SetChannel(uint8_t chBitmask);

private:
    uint8_t EnableGpioPin(uint8_t pin);
    void    ClearMeasData();

    AdcIf    *adcIf;
    uint8_t   chGpioPins[numChannelBits];
    bool      gpioConfigured;
    MeasData *measData;
};

}

#endif
