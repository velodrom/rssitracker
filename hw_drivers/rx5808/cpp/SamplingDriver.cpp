#include <stdio.h>
#include <sys/time.h>
#include <signal.h>
#include <stdlib.h>

static int counter = 0;

void HandleAlarm(int sig)
{
    counter++;
}

void HandleExit(int sig)
{
    printf("SIGALRMs received: %d\n", counter);
    exit(0);
}

int main(void)
{
    printf("Starting interrupt timer..\n");

    struct itimerval timer;
    timer.it_interval.tv_sec = 0;
    timer.it_interval.tv_usec = 10000; // 100 samples per second
    timer.it_value.tv_sec = 0;
    timer.it_value.tv_usec= 10; // Time until first sample is captured

    signal(SIGALRM, HandleAlarm);
    signal(SIGINT, HandleExit);
    
    int retval = setitimer(ITIMER_REAL, &timer, NULL);
    if (retval != 0)
        printf("Failed to set timer: %d\n", retval);
    while(1);
}