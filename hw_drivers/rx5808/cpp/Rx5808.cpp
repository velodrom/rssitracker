#include <stdio.h>
#include <wiringPi.h>
#include "Rx5808.hpp"

Rx5808::Rx5808(AdcIf *newAdcIf) :
adcIf(newAdcIf),
gpioConfigured(false)
{
    for (int idx = 0; idx < numChannelBits; idx++)
    {
        chGpioPins[idx] = 0;
    }

    ClearMeasData();
}

Rx5808::~Rx5808()
{
    for (int idx = 0; idx < numChannelBits; idx++)
    {
        chGpioPins[idx] = 0;
    }
}

MeasData *Rx5808::Measure(const MeasParams &params)
{
    MeasData *returnPtr = nullptr;
    if (IsConfigured())
    {
        SetChannel(params.rssiParams.chBitmask);
        uint32_t digitalRssi = adcIf->Measure();

        measData->rssiMeasData.rssiMeas[measData->rssiMeasData.numRssiMeas] = digitalRssi;
        measData->rssiMeasData.numRssiMeas++;
    }

    if (measData->rssiMeasData.numRssiMeas >= params.rssiParams.measPeriod)
    {
        returnPtr = measData;
        ClearMeasData();
    }
    return returnPtr;
}

void Rx5808::Configure(const uint8_t chPins[numChannelBits])
{
    if (wiringPiSetup() != -1)
    {
        gpioConfigured = true;

        for (int idx = 0; idx < numChannelBits; idx++)
        {
            chGpioPins[idx] = EnableGpioPin(chPins[idx]);
        }
    }
}

bool Rx5808::IsConfigured()
{
    // Concrete AdcIf is injected as dependency
    // and must be configured by injector
    return gpioConfigured && adcIf->IsConfigured();
}

uint8_t Rx5808::EnableGpioPin(uint8_t pin)
{
    pinMode(pin, OUTPUT);
    return pin;
}

void Rx5808::SetChannel(uint8_t chBitmask)
{
    for (uint16_t chIdx = 0; chIdx < numChannelBits; chIdx++)
    {
        uint8_t pinState = ((chBitmask & (0x1 << chIdx)) == 1) ? HIGH : LOW;
        digitalWrite(chGpioPins[chIdx], pinState);
    }
}

void Rx5808::ClearMeasData()
{
    measData = new MeasData();
    measData->rssiMeasData.numRssiMeas = 0;
}
