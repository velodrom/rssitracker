#ifndef PHY_MEAS_IF
#define PHY_MEAS_IF

#include "MeasTypes.hpp"

class PhyMeasIf
{
public:
    virtual MeasData *Measure(const MeasParams &params) = 0;
    virtual bool      IsConfigured() = 0;
};

#endif
