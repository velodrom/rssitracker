#ifndef RXWRAPPER_HPP
#define RXWRAPPER_HPP

#include "Rx5808.hpp"
#include "Mcp3204.hpp"

extern "C"
{
typedef struct RxChGpioPins
{
	uint8_t chPins[numChannelBits];
} RxChGpioPins;

extern Rx5808* Rx5808_new(AdcIf* adcIf);
extern void Rx5808_config(Rx5808* rxObj, RxChGpioPins gpioCfg);
extern MeasData Rx5808_measureRssi(Rx5808* rxObj, uint8_t chBitMask);
extern Mcp3204* Mcp3204_new();
extern uint32_t RunOneMeasurement(uint8_t chBitmask);
extern uint32_t TestMcp3204(AdcIf* adc);
}

#endif //RXWRAPPER_HPP
