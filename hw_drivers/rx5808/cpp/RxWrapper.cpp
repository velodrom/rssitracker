#include <stdio.h>
#include "RxWrapper.hpp"

extern "C"
{
Rx5808* Rx5808_new(AdcIf* adcIf)
{
    return new Rx5808(adcIf);
}

void Rx5808_config(Rx5808* rxObj, RxChGpioPins gpioCfg)
{
    rxObj->Configure(gpioCfg.chPins);
}

MeasData Rx5808_measureRssi(Rx5808* rxObj, uint8_t chBitmask)
{
    MeasParams params = { .rssiParams = {
        chBitmask : chBitmask,
        measPeriod : 1
    }};
    return rxObj->Measure(params)[0];
}

Mcp3204* Mcp3204_new()
{
    Mcp3204* mcpAdc = new Mcp3204();
    mcpAdc->Configure();
    return mcpAdc;
}

uint32_t RunOneMeasurement(uint8_t chBitmask)
{
    const uint8_t chPins[numChannelBits] = {16,20,21};
    Mcp3204* mcpAdc   = new Mcp3204();
    mcpAdc->Configure();

    Rx5808* rx = new Rx5808(mcpAdc);
    rx->Configure(chPins);

    MeasParams params = { .rssiParams = {
        chBitmask : chBitmask,
        measPeriod : 1
    }};
    MeasData *rxMeas = rx->Measure(params);
    printf("RunOneMeasurements %d\n", chBitmask);
    return rxMeas->rssiMeasData.rssiMeas[0];
}

uint32_t TestMcp3204(AdcIf* adc)
{
    uint32_t meas = adc->Measure();
    return meas;
}
}
