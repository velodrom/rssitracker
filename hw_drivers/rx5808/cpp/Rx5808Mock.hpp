#ifndef RX5808_MOCK_HPP
#define RX5808_MOCK_HPP

#include "PhyMeasIf.hpp"

class RssiMeasMock : public PhyMeasIf
{
public:
    RssiMeasMock():
        numMeasReturned(0) {}

#pragma GCC diagnostic ignored "-Wunused-parameter"
    MeasData *Measure(const MeasParams &params)
    {
        MeasData *data = new MeasData();
        data->rssiMeasData = {
            numRssiMeas : 1,
            rssiMeas : {0x1}
        };

        numMeasReturned++;
        return data;
    }

    bool IsConfigured()
    {
        return true;
    }

    uint32_t numMeasReturned;
};

#endif
