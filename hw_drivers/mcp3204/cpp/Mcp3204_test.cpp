#include "gtest/gtest.h"
#include "AdcIf.hpp"
#include "Mcp3204.hpp"

TEST(Mcp3204Test, ClassCreates)
{
    Mcp3204 *adc = new Mcp3204();
    ASSERT_TRUE(adc != 0);
}

TEST(Mcp3204Test, ConfigureAndOpenDevice)
{
    // Real hardware required for this unit test
    Mcp3204 *adc = new Mcp3204();
    ASSERT_TRUE(adc->Configure());
}

TEST(Mcp3204Test, CreateMeasRequest)
{
    Mcp3204 *adc                = new Mcp3204();
    struct spi_ioc_transfer msg = adc->CreateMeasurementRequest();

    uint32_t *txBuf32   = reinterpret_cast<uint32_t*>(msg.tx_buf);
    uint32_t byteMask3B = 0x00FFFFFF;
    ASSERT_EQ(0x0006, (*txBuf32) & byteMask3B); // 0x06000 little endian -> 0x0006

    adc->DeleteMeasurementRequest(msg);
}

TEST(Mcp3204Test, ExtractMeasurement)
{
    Mcp3204 *adc = new Mcp3204();
    uint8_t txBuf[] = {0x06, 0x00, 0x00};
    uint8_t rxBuf[] = {0x06, 0x04, 0x56};

#pragma GCC diagnostic ignored "-Wmissing-field-initializers"
    SpiTransfer txMsg =
    {
        tx_buf   : (__u64)txBuf,
        rx_buf   : (__u64)rxBuf
    };

    uint32_t measurement = adc->ExtractMeasurement(txMsg);
    ASSERT_EQ(0x000456, measurement);
}

TEST(Mcp3204Test, RequestMeasurement)
{
    // Real hardware required for this unit test
    Mcp3204  *adc = new Mcp3204();
    adc->Configure();

    uint32_t rxMeas;
    rxMeas = adc->Measure();

    ASSERT_TRUE(rxMeas > 0);
}
