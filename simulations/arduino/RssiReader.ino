int rssiPin = A0;
int ch1Pin  = 1;
int ch2Pin  = 2;
int ch3Pin  = 3;

void setup() {
  // put your setup code here, to run once:
  pinMode(ch1Pin, OUTPUT);
  pinMode(ch2Pin, OUTPUT);
  pinMode(ch3Pin, OUTPUT);

  Serial.begin(115200);
}

void loop() {
  // put your main code here, to run repeatedly:
  setChannel(0,0,0);
  int rawRssi = readRssi();
  Serial.println(rawRssi);
}

void setChannel(int ch1, int ch2, int ch3)
{
  digitalWrite(ch1Pin, ch1);
  digitalWrite(ch2Pin, ch2);
  digitalWrite(ch3Pin, ch3);
}

int readRssi(void)
{
  int cumRssi = 0;
  int numSamples = 3;
  for (int samples = 0; samples < numSamples; samples++)
  {
    cumRssi += analogRead(rssiPin);
    delay(10);
  }
  return cumRssi/numSamples;
}

