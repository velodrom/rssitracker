figure;
fs = 34; % Sampling frequency
timeVec = [0:length(rawRssi)-1]/fs;

%% Remove DC component
noDcRssi = rawRssi - min(rawRssi);
subplot(3,1,1); plot(timeVec, noDcRssi);
xlabel('Time (s)'); ylabel('RSSI (raw)'); grid on;

%% Filter signal FIR
load('fir48_34hz.mat');
firCoefs = fir48_34hz;
firGroupDelay = grpdelay(firCoefs,1,1,fs);
firRssi = filter(firCoefs,1,noDcRssi);

load('iir5_34hz.mat');
iirRssi = filter(b,a,noDcRssi);
iirGroupDelay = ceil(grpdelay(b,a,1,fs));
hold on; plot(timeVec, [firRssi(firGroupDelay:end) zeros(1,firGroupDelay-1)], 'b');
hold on; plot(timeVec, [iirRssi(iirGroupDelay:end) zeros(1,iirGroupDelay-1)], 'r');
legend('raw','firRssi','iirRssi');

%% Normalize signal
maxVal = max(firRssi);
normSmoothRssi = firRssi/maxVal;
subplot(3,1,2); plot(timeVec, normSmoothRssi);

%% Differentiate signal
rssiD = diff(firRssi);
% hold on; plot(timeVec(1:end-1), rssiD, 'k');

%% Integrate
windowLen = fs;
rssiI = filter(ones(1,windowLen),1,rssiD);
rssiI = rssiI/max(rssiI);
hold on; plot(timeVec(1:end-1), rssiI);

%% Calculate mean
% meanVal = mean(normSmoothRssi);
% hold on; plot(timeVec, meanVal*ones(length(timeVec)),'r')

%% Plot histogram of values
nBins = 50;
rssiHist = hist(normSmoothRssi, nBins);
% hold on; barh(linspace(0,1,nBins), 5*rssiHist/max(rssiHist));
hold on; plot(5*rssiHist/max(rssiHist), linspace(0,1,nBins),'g');

%% Find peaks
threshold = 0.7; % Per-cent of maximum value
[pks,locs] = findpeaks(normSmoothRssi,...
                       'NPeaks',10,...
                       'MinPeakDistance',3*fs,...
                       'MinPeakHeight', threshold);
hold on; plot(timeVec(locs), pks, 'or')
hold on; plot(timeVec, threshold*ones(length(timeVec)), 'r');

%% Display lap times
peakTimes = locs/fs;
lapTimes  = diff(peakTimes);
for lap = 1:length(lapTimes)
    disp(['Lap ', num2str(lap), ': ', num2str(lapTimes(lap))]);
end

%% K-means clustering of rssi data
% [clusteredData, clusterCenters] = kmeans(smoothRssi, 100);
% figure; plot(clusteredData)

% NFFT = 2^8;
% fft(normSmoothRssi,NFFT);
% subplot(3,1,3); plot(20*log10(fftshift(abs(fft(normSmoothRssi)).^2)))