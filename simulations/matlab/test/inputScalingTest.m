function filterTest()
    testFilteringWithOneDataBlock();
    testFilteringWithTwoConsecutiveDataBlocks();
end

function testFilteringWithOneDataBlock()
    load 'fd_calibrate_2laps.mat' 'rawRssi';
    load 'fir10_34hz.mat' 'fir10_34hz';
    fs = 34;
    nSamples = 10;
    rssiData = rawRssi(1:nSamples*fs);
    
    refFiltRssi = filter(fir10_34hz,1,rssiData);
    filtRssi = filterRssi(fir10_34hz, rssiData);
    
    checkEqual(filtRssi, refFiltRssi, 0.5);
end

function testFilteringWithTwoConsecutiveDataBlocks()
    load 'fd_calibrate_2laps.mat' 'rawRssi';
    load 'fir10_34hz.mat' 'fir10_34hz';
    fs = 34;
    nBlocks  = 2;
    nSamples = 10;
    blockSize = nSamples*fs;
    rssiData = rawRssi(1:(blockSize*nBlocks));
    
    refFiltRssi = filter(fir10_34hz,1,rssiData);
    filtRssi = filterRssi(fir10_34hz, rssiData(1:blockSize));
    filtRssi = [filtRssi filterRssi(fir10_34hz, rssiData(blockSize+1:blockSize*2))];
    
    checkEqual(filtRssi, refFiltRssi, 0.5);
end

function filtRssi = filterRssi(coefs, rssiData)
    filtRssi    = zeros(1,length(rssiData));
    filterInput = zeros(1,length(coefs));

    for sample = 1:length(rssiData)
        filterInput(1:end-1) = filterInput(2:end);
        filterInput(end) = rssiData(sample);
        filtRssi(sample) = sum(filterInput.*fliplr(coefs));
    end
end

function checkEqual(output, reference, tol)
        error = abs(output - reference);
    [val,idx] = find(error > tol);
    if length(idx) > 0
        disp(['FAIL ', num2str(error(idx))]);
        figure; plot([output; reference].');
        legend('output','reference');
    end
end