var io = require('socket.io-client');

var socket = io('http://localhost:8000');

socket.on('connect', function(){
	console.log('Connected');
	socket.emit('subscribe', {client: '11'}, function(){
		console.log('Subscribed for measurements!');
	});

socket.on('rssi_data', function(){
	console.log('Got rssi data');
	});
});

