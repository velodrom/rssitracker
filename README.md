# vTx lap timer #


This project is a lap timer system designed primarily for multicopter racing. 


The system relies on FPV cameras and video transmitters typically used in multicopters that are designed for racing. By measuring the transmitted video signal strength RSSI value, and placing the video receiver module close to the finish line, we can keep track of laps and lap times.


### Hardware requirements ###

* Raspberry Pi 3
* RX5808 5.8GHz video receiver module
* MCP3204 A/D converter


The currently selected hardware components support simultaneous tracking by means of receiver frequency hopping, or installing multiple receivers. One MCP3204 can sample 4 receiver channels simultaneously.

### Software requirements ###

* Python 2.7 or higher with following libraries

1. python-socketio (back-end to front-end communications)
2. kombu (socketio server message router)
3. redis (message queue database)
4. eventlet

* Node.js framework with socket.io
* WiringPi library for C/C++ (GPIO interaction with hardware, http://wiringpi.com/wiringpi-updated-to-2-36/)
* Redis database server (https://redis.io/)

The RX5808 and MCP3204 hardware drivers are written in C++, and compiled into a dynamic library.