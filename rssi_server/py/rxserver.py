import eventlet
eventlet.monkey_patch() # Monkey patch socketio for redis
import time
import socketio
import logging
from socketio_server import SocketioServer
from RxWrapper import RxWrapper

logging.basicConfig(filename='rxserver.log', level=logging.INFO)
logger = logging.getLogger(__name__)

if __name__ == "__main__":
    sio = SocketioServer(host='localhost', port=8000)
    sio.start_server() # Server is started in a dedicated thread

    time.sleep(1) # Wait for the server to start up

    external_sio = socketio.KombuManager('redis://', write_only=True)

    rx5808     = RxWrapper()
    gpioChPins = [16,20,21]
    rx5808.Configure(gpioChPins)
    while (1):
        time.sleep(1)
        rssi = rx5808.MeasureRssi()
        logger.info('Acquiring measurements')
        external_sio.emit('rssi_data', data={'rssi': rssi})