import eventlet
eventlet.monkey_patch() # Monkey patch socketio for redis
import threading
import socketio
import logging
from flask import Flask, render_template


logger = logging.getLogger(__name__)

class SocketioServer(socketio.Namespace):
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.app      = Flask(__name__)
        self.queueMgr = socketio.KombuManager('redis://')
        #self.sio      = socketio.Server(client_manager=self.queueMgr, async_mode='threading')
        self.sio      = socketio.Server(client_manager=self.queueMgr)

        # wrap Flask application with socketio's middleware
        self.app = socketio.Middleware(self.sio, self.app)
        #self.app.wsgi_app = socketio.Middleware(self.sio, self.app.wsgi_app)
        self.thread = []

    def start_server(self):
        self.thread = threading.Thread(name='rxserver', target=self.launch_server_thread)
        self.thread.setDaemon(True)
        self.thread.start()
        #self.launch_server_thread()

    def launch_server_thread(self):
        # deploy as an eventlet WSGI server
        # self.app.run(host='localhost', port=8000, threaded=True)
        logger.info('Starting eventlet server on %s:%s', self.host, self.port)
        eventlet.wsgi.server(eventlet.listen((self.host, 8000)), self.app)
        logger.warning('WARNING: RETURNED FROM EVENTLET.WSGI.SERVER()')

    def on_index(self):
        #Serve the client-side application.
        return render_template('index.html')

    def on_connect(self, sid, environ):
        logger.info('connect ', sid)

    def on_subscribe(self, sid, data):
        logger.info('subscribe. ', data)

    def on_disconnect(self, sid):
        logger.info('disconnect ', sid)

    def on_rssi_data(self, sid, data):
        # ToDo: Why are these events not triggered in threading mode?
        logger.info('Received data' + str(data))
