import threading
import sys

def print_something():
	#with open('testfile.txt','w') as f:
	#	f.write('Skip')
	#	f.close()
	print('Test')
	return

def start_thread():
	thread = threading.Thread(target = print_something)
	thread.start()
	thread.join(2.0)

if __name__ == "__main__":
	start_thread()
	print('Finished')
	sys.stdout.flush()