#Socket.IO server
pip install python-socketio

#Message queue service provider
# Needed for emitting events 
# through the socketio server
pip install kombu

#Redis queues on top of Kombu
pip install redis
