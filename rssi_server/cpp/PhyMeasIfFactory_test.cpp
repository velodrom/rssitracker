#include <gtest/gtest.h>
#include "PhyMeasIfFactory.hpp"

using namespace ::testing;

class PhyMeasIfFactoryTest : public Test
{
public:
    PhyMeasIfFactoryTest()
    {
        phyMeasFactory = new PhyMeasIfFactory();
    }

    PhyMeasIfFactory *phyMeasFactory;
};

TEST_F(PhyMeasIfFactoryTest, CreateRssiMeasObject)
{
    Rx5808 *phyRssiObj = dynamic_cast<Rx5808*>(phyMeasFactory->CreatePhyMeasObj(RssiMeasType));
    ASSERT_TRUE(phyRssiObj != nullptr);
}
