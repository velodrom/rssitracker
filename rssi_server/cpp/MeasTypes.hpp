#ifndef MEAS_TYPES_HPP
#define MEAS_TYPES_HPP
#include <stdint.h>
#include <vector>

using namespace std;
const uint32_t maxNumMeasurements = 40;

typedef enum MeasName
{
    RssiMeasType     = 0,
    RssiFromFileType = 1,
    MaxNumMeasuremets = 2
} MeasName;

typedef uint32_t RssiMeas;

typedef struct SRssiMeasData
{
    uint32_t numRssiMeas;
    RssiMeas rssiMeas[maxNumMeasurements];
} SRssiMeasData;

typedef union MeasData
{
    SRssiMeasData rssiMeasData;
} MeasData;

typedef struct SRssiMeasParams {
    uint8_t  chBitmask;
    uint32_t measPeriod;
} SRssiMeasParams;

typedef union MeasParams
{
    SRssiMeasParams rssiParams;
} MeasParams;

#endif
