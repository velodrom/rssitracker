#ifndef LAP_TIMER_HPP
#define LAP_TIMER_HPP
#include <vector>
#include "MeasObserver.hpp"
#include "LapController.hpp"
#include "MeasTypes.hpp"
#include "FirFilter.hpp"

using namespace std;

const uint32_t maxSampleBufferSize = (1 << 15); // 128 KB

class LapTimer : public MeasObserver
{
public:
    LapTimer(MeasParams params, LapController &lapCntrl);
    ~LapTimer();
    void       MeasReady(MeasData *dataPtr);
    MeasParams GetMeasParams();
    void       SetMeasPeriod(uint32_t period);
    uint32_t   GetSampleCount();
    uint32_t   CalibrateMaxSignalLevel(uint32_t *rssiData, uint32_t numSamples);

    vector<RssiMeas> GetLastNSamples(const uint32_t numSamples);
    vector<uint32_t> peakRisingEdgeIdx;
    vector<uint32_t> peakFallingEdgeIdx;
private:
    void SaveSamples(MeasData *dataPtr);
    void SaveSamples(RssiMeas *data, uint32_t numNewSamples);
    void DetectPeakEdges(uint32_t *rssiData, uint32_t numSamples);

protected:
    RssiMeas rawSampleBuffer[maxSampleBufferSize];
    uint32_t currentSampleIdx;
    uint32_t totalNumSamples;

private:
    LapController   &lapCntrl;
    RssiMeas        *newSamplePtr;
    MeasParams       measParams;
    FirFilter       *firFilter;
    uint32_t         maxRssi;
    float            peakThreshold;
    bool             findFallingEdge;

};

#endif
