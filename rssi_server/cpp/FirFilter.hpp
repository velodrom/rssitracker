#ifndef FIR_FILTER_HPP
#define FIR_FILTER_HPP
#include "MeasTypes.hpp"

const uint32_t bufferSize = (1<<6);
const uint32_t filtLen    = 6;
const uint32_t coefs[filtLen] = {1386, 6338, 10926, 10926, 6338, 1386};// Q16.15
const uint32_t qFormat = 15;

class FirFilter
{
public:
    FirFilter();
    ~FirFilter();
    void Filter(uint32_t * input, uint32_t *output, uint32_t inputLen); // In place operation

private:
    void SaveState(uint32_t *inputState);
    void CopyInput(uint32_t *input, uint32_t inputLen);

    uint32_t stateBuf[bufferSize];
};

#endif
