#include <stdint.h>
#include "PhyMeasIfFactory.hpp"
#include "Mcp3204.hpp"
#include "Mcp3204FromFile.hpp"
#include "GlobalDefs.hpp"

PhyMeasIf *PhyMeasIfFactory::CreatePhyMeasObj(MeasName measName)
{
    PhyMeasIf *retObj = nullptr;
    switch(measName)
    {
        case RssiMeasType:
            retObj = CreateRx5808Obj();
            break;
        case RssiFromFileType:
            retObj = CreateRx5808FileStreamObj();
            break;
        case MaxNumMeasuremets:
            // Flow through to default
        default:
            break;
    }
    return retObj;
}

Rx5808* PhyMeasIfFactory::CreateRx5808Obj()
{
    Rx5808  *rx5808  = nullptr;
    Mcp3204 *mcp3204 = new Mcp3204();

    bool adcOk = mcp3204->Configure();
    if (adcOk)
    {
       rx5808 = new Rx5808(mcp3204);
       const uint8_t chPins[numChannelBits] = {16,20,21};
       rx5808->Configure(chPins);
    }
    return rx5808;
}

Rx5808* PhyMeasIfFactory::CreateRx5808FileStreamObj()
{
    Rx5808          *rx5808 = nullptr;
    Mcp3204FromFile *mcp3204f = new Mcp3204FromFile(rssiOutputFile);

    bool adcOk = mcp3204f->Configure();
    if (adcOk)
    {
        rx5808 = new Rx5808(mcp3204f);
        const uint8_t chPins[numChannelBits] = {16,20,21};
        rx5808->Configure(chPins);
    }
    return rx5808;
}

