#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include "MeasController.hpp"

MeasController::MeasController()
{
}

bool MeasController::RegisterObserver(MeasObserver *observer, MeasName measName)
{
    bool registerOk = false;
    if (not MeasurementExists(measName))
    {
        PhyObserverVector *phyMeasObsVector = new PhyObserverVector();
        phyMeasMap[measName] = phyMeasObsVector;
    }

    PhyMeasObserverPair *phyObsPair = new PhyMeasObserverPair();
    PhyMeasIf *phyMeasIf = phyMeasFactory.CreatePhyMeasObj(measName);
    if (phyMeasIf != nullptr)
    {
        phyObsPair->phyMeasIf    = phyMeasIf;
        phyObsPair->measObserver = observer;
        PhyObserverVector *phyObsVector = phyMeasMap[measName];
        phyObsVector->push_back(phyObsPair);
        registerOk = true;
    }

    return registerOk;
}

void MeasController::UnRegisterObserver(MeasObserver *observer, MeasName measName)
{
    PhyObserverVector *phyObsVector = phyMeasMap[measName];
    PhyObserverVector::iterator item = phyObsVector->begin();

    for (; item != phyObsVector->end(); item++)
    {
        if ((*item)->measObserver == observer)
        {
            phyObsVector->erase(item);
            break;
        }
    }
}

void MeasController::TriggerAllMeasurements()
{
    PhyMeasMap::iterator phyMeasObs = phyMeasMap.begin();
    for (;phyMeasObs != phyMeasMap.end(); phyMeasObs++)
    {
        TriggerMeasurementsFor(phyMeasObs->second);
    }
}

void MeasController::TriggerMeasurementsFor(PhyObserverVector* phyMeasObsVect)
{
    PhyObserverVector::iterator phyObsIter = phyMeasObsVect->begin();
    for (; phyObsIter != phyMeasObsVect->end(); phyObsIter++)
    {
        PhyMeasIf    *phyMeas = (*phyObsIter)->phyMeasIf;
        MeasObserver *measObs = (*phyObsIter)->measObserver;
        MeasData *data = phyMeas->Measure(measObs->GetMeasParams());

        if (data != nullptr)
        {
            measObs->MeasReady(data);
        }
    }
}

bool MeasController::MeasurementExists(MeasName measName)
{
    bool measExists = false;
    PhyMeasMap::iterator iter = phyMeasMap.find(measName);
    if (iter != phyMeasMap.end())
    {
        measExists = true;
    }

    return measExists;
}

uint32_t MeasController::NumRegObservers(MeasName measName)
{
    return phyMeasMap[measName]->size();
}

uint32_t MeasController::NumMeasurements()
{
    return phyMeasMap.size();
}
