#ifndef LAP_CONTROLLER_HPP
#define LAP_CONTROLLER_HPP
#include <vector>
#include "MeasTypes.hpp"

const uint32_t maxSamplingTimeUs = 1000000; // 1 second

class LapController
{
public:
    LapController();
    void SetSamplingTime(uint32_t ts);
    uint32_t GetSamplingTime();
    uint32_t GetSamplingFreq();

    bool recalibrate;
    uint32_t minPeakRisingEdgeDist; // in seconds
    uint32_t minPeakFallingEdgeDist; // in seconds
private:
    uint32_t samplingTimeUs;
};

#endif
