#include <gtest/gtest.h>
#include <unistd.h>
#include <stdint.h>
#include "MeasController.hpp"
#include "MeasObserverMock.hpp"
#include "MeasTypes.hpp"
#include "Rx5808Mock.hpp"

using namespace ::testing;

class MeasControllerTest : public Test
{
public:
    MeasControllerTest()
    {
        measCntrl = new MeasController();
    }
protected:
    MeasController *measCntrl;
};

TEST_F(MeasControllerTest, NoMeasurementExistsAtStart)
{
    bool measExists = measCntrl->MeasurementExists(RssiMeasType);
    ASSERT_FALSE(measExists);
}

TEST_F(MeasControllerTest, RegisterObserverToRssiMeasurement)
{
    MeasObserverMock *observerMock = new MeasObserverMock();
    measCntrl->RegisterObserver(observerMock, RssiMeasType);

    uint32_t numObservers = measCntrl->NumRegObservers(RssiMeasType);

    ASSERT_EQ(1, numObservers);
}

TEST_F(MeasControllerTest, RegisterMultipleObserversSucceeds)
{
    MeasObserverMock *observerMock1 = new MeasObserverMock();
    MeasObserverMock *observerMock2 = new MeasObserverMock();
    measCntrl->RegisterObserver(observerMock1, RssiMeasType);
    measCntrl->RegisterObserver(observerMock2, RssiMeasType);

    uint32_t numObservers = measCntrl->NumRegObservers(RssiMeasType);

    ASSERT_EQ(2, numObservers);
}

TEST_F(MeasControllerTest, UnregisterObserver)
{

    MeasObserverMock *observerMock = new MeasObserverMock();
    measCntrl->RegisterObserver(observerMock, RssiMeasType);

    uint32_t numObservers = measCntrl->NumRegObservers(RssiMeasType);
    ASSERT_EQ(1, numObservers);

    measCntrl->UnRegisterObserver(observerMock, RssiMeasType);
    numObservers = measCntrl->NumRegObservers(RssiMeasType);

    ASSERT_EQ(0, numObservers);
}

TEST_F(MeasControllerTest, NewMeasDataTriggersObservers)
{
    MeasObserverMock *observerMock = new MeasObserverMock();
    measCntrl->RegisterObserver(observerMock, RssiMeasType);
    ASSERT_EQ(0, observerMock->numReportedMeas);

    measCntrl->TriggerAllMeasurements();

    ASSERT_EQ(1, observerMock->numReportedMeas);
}

