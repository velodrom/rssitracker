#include <gtest/gtest.h>
#include <stdint.h>
#include "LapTimer.hpp"
#include "MeasController.hpp"

using namespace ::testing;

class LapControllerMock : public LapController
{
public:
    LapControllerMock() : calibrate(true) {};
    bool calibrate;
};

class LapTimerTest : public Test
{
public:
    LapTimerTest()
    {
        MeasParams measParams = {.rssiParams = {
                chBitmask : 0x0,
                measPeriod : 1
        }};
        lapCntrl = new LapControllerMock();
        lapTimer = new LapTimer(measParams, *lapCntrl);

    }
protected:
    LapController  *lapCntrl;
    LapTimer       *lapTimer;
};

TEST_F(LapTimerTest, AccumulateMeasurements)
{
    MeasData data = {
            .rssiMeasData = {
                numRssiMeas : 10,
                rssiMeas    : {0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0x10}
    }};
    lapTimer->MeasReady(&data);

    ASSERT_EQ(data.rssiMeasData.numRssiMeas, lapTimer->GetSampleCount());
}

TEST_F(LapTimerTest, LastSampleIsZeroAfterInitialization)
{
    std::vector<RssiMeas> meas = lapTimer->GetLastNSamples(1);
    ASSERT_EQ(0, meas.size());
}

TEST_F(LapTimerTest, ReturnPreviousSample)
{
    MeasData data;
    uint32_t expectedSample = 12;
    data.rssiMeasData.numRssiMeas = 1;
    data.rssiMeasData.rssiMeas[0] = expectedSample;
    lapTimer->MeasReady(&data);
    std::vector<RssiMeas> meas = lapTimer->GetLastNSamples(1);
    ASSERT_EQ(expectedSample, meas[0]);
}

TEST_F(LapTimerTest, ExceedingMaxMeasCountShouldWork)
{
    MeasData data;
    data.rssiMeasData.numRssiMeas = maxNumMeasurements;
    for (uint32_t dataIdx = 0; dataIdx < maxNumMeasurements; dataIdx++)
        data.rssiMeasData.rssiMeas[dataIdx] = dataIdx;

    // Fill observer internal buffer of size: maxSampleBufferSize
    uint32_t sampleCount = 0;
    for (; sampleCount+maxNumMeasurements <= maxSampleBufferSize; sampleCount += maxNumMeasurements)
        lapTimer->MeasReady(&data);

    // Send one measurement more
    printf("SampleCount: %d, plim: %d\n",sampleCount, lapTimer->GetSampleCount());
    uint32_t expectedLastSample   = 11;
    uint32_t lastSampleIndex      = maxSampleBufferSize-sampleCount+1;
    data.rssiMeasData.numRssiMeas = lastSampleIndex;
    data.rssiMeasData.rssiMeas[lastSampleIndex-1] = expectedLastSample;
    lapTimer->MeasReady(&data);

    EXPECT_EQ(sampleCount+lastSampleIndex, lapTimer->GetSampleCount());
    EXPECT_EQ(expectedLastSample, (lapTimer->GetLastNSamples(1))[0]);
}

TEST_F(LapTimerTest, ReturnDataWrappedAroundInternalBuffer)
{
    uint32_t numMeas = maxNumMeasurements;
    MeasData data;
    data.rssiMeasData.numRssiMeas = numMeas;
    for (uint32_t dataIdx = 0; dataIdx < numMeas; dataIdx++)
        data.rssiMeasData.rssiMeas[dataIdx] = dataIdx;

    // Fill observer internal buffer of size: maxSampleBufferSize
    uint32_t sampleCount = 0;
    for (; sampleCount <= maxSampleBufferSize; sampleCount += numMeas)
        lapTimer->MeasReady(&data);

    uint32_t consecutiveSampleDiff = 1;
    uint32_t wrapSampleCount       = sampleCount - maxSampleBufferSize;
    std::vector<RssiMeas> meas     = lapTimer->GetLastNSamples(wrapSampleCount + 1);

    ASSERT_EQ(consecutiveSampleDiff,
              (meas[wrapSampleCount] - meas[wrapSampleCount-1]));
    ASSERT_EQ(wrapSampleCount+1, meas.size());
}

TEST_F(LapTimerTest, CalibrateMaximumSignalLevelFromStableSignal)
{
    const uint32_t numSamples = 20;
    uint32_t filteredRssi[numSamples] = {625, 625, 627, 630, 629, 625, 622, 620, 620, 620, 620, 621, 622, 624, 626, 626, 624, 620, 617, 616};
    uint32_t refMaxRssi = 630;

    uint32_t maxRssi = lapTimer->CalibrateMaxSignalLevel(&filteredRssi[0], numSamples);
    ASSERT_EQ(refMaxRssi, maxRssi);
}

TEST_F(LapTimerTest, DetectFirstFallingEdge)
{
    uint32_t numMeas = 200;
    MeasData data;
    uint32_t rssiValue = 511;
    data.rssiMeasData.numRssiMeas = maxNumMeasurements;
    for (uint32_t dataIdx = 0; dataIdx < maxNumMeasurements; dataIdx++)
        data.rssiMeasData.rssiMeas[dataIdx] = rssiValue;
    lapTimer->MeasReady(&data); // Calibrate
    lapCntrl->recalibrate = false;

    for (uint32_t measCount = 0; measCount < numMeas; measCount += maxNumMeasurements) {
        for (uint32_t dataIdx = 0; dataIdx < maxNumMeasurements; dataIdx++) {
            data.rssiMeasData.rssiMeas[dataIdx] = rssiValue;
            rssiValue -= 1;
        }
        lapTimer->MeasReady(&data);
    }

    ASSERT_TRUE(lapTimer->peakFallingEdgeIdx.size() > 0);
}
//TEST_F(LapTimerTest, )
