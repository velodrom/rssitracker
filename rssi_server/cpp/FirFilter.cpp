#include <cstring>
#include <stdio.h>
#include "FirFilter.hpp"

FirFilter::FirFilter()
{
    std::memset(stateBuf, 0, bufferSize*sizeof(uint32_t));
}

void FirFilter::Filter(uint32_t *input, uint32_t *output, uint32_t inputLen)
{
    const uint32_t *coefPtr = &coefs[0];
    uint32_t *inputPtr = &stateBuf[0];
    CopyInput(input, inputLen);

    for (uint32_t inputIdx = 0; inputIdx < inputLen; inputIdx++)
    {
        uint32_t currentSampleSum = 0;
        for (uint32_t coefIdx = 0; coefIdx < filtLen; coefIdx++)
        {
            currentSampleSum += coefPtr[coefIdx]*inputPtr[coefIdx];
        }
        output[inputIdx] = currentSampleSum >> qFormat;
        inputPtr++; // Advance input in steady state filter
    }
    SaveState(inputPtr);
}

void FirFilter::SaveState(uint32_t *inputState)
{
    std::memcpy(&stateBuf[0],
                inputState,
                (filtLen-1)*sizeof(uint32_t));
}

void FirFilter::CopyInput(uint32_t *input, uint32_t inputLen)
{
    std::memcpy(&stateBuf[filtLen-1],
                input,
                inputLen*sizeof(uint32_t));
}
