#include "gtest/gtest.h"
#include "Mcp3204FromFile.hpp"
#include <string>
#include <stdio.h>

using namespace ::testing;

class Mcp3204FromFileTest : public Test
{
public:
    Mcp3204FromFileTest()
    {
        fileName = "testRssiInputStream.txt";
    }

    ~Mcp3204FromFileTest()
    {
        remove(fileName.c_str());
    }

    string fileName;

    void GenerateInputData(uint32_t numSamples)
    {
        pFile = fopen(fileName.c_str(), "w");
        if (pFile != NULL)
        {
            for (uint32_t sampleNum = 1; sampleNum <= numSamples; sampleNum++)
            {
                uint32_t randRssi = sampleNum;
                fprintf(pFile, "rssi: %d\n", randRssi);
            }
            fclose(pFile);
        }
        else
            printf("Failed to open %s\n", fileName.c_str());
    }

private:
    FILE*  pFile;
};

TEST_F(Mcp3204FromFileTest, ReadOneSampleSucceeds)
{
    GenerateInputData(1);
    Mcp3204FromFile *mcp3204f = new Mcp3204FromFile(fileName);
    uint32_t rssi = mcp3204f->Measure();
    uint32_t refRssi = 1;
    EXPECT_EQ(refRssi, rssi);
}

TEST_F(Mcp3204FromFileTest, ReadTwoSamples1And2)
{
    GenerateInputData(2);
    Mcp3204FromFile *mcp3204f = new Mcp3204FromFile(fileName);

    uint32_t rssi = mcp3204f->Measure();
    uint32_t refRssi = 1;
    EXPECT_EQ(refRssi, rssi);

    rssi = mcp3204f->Measure();
    refRssi = 2;
    EXPECT_EQ(refRssi, rssi);
}

TEST_F(Mcp3204FromFileTest, ReadingPastFileEndReturnsFirstValueInFile)
{
    GenerateInputData(1);
    Mcp3204FromFile *mcp3204f = new Mcp3204FromFile(fileName);

    uint32_t rssi = mcp3204f->Measure();
    uint32_t refRssi = 1;
    EXPECT_EQ(refRssi, rssi);

    rssi = mcp3204f->Measure();
    refRssi = 1;
    EXPECT_EQ(refRssi, rssi);
}
