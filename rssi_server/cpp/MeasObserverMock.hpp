#ifndef MEAS_OBSERVER_MOCK_HPP
#define MEAS_OBSERVER_MOCK_HPP

#include "MeasObserver.hpp"

class MeasObserverMock : public MeasObserver
{
public:
    MeasObserverMock()
    {
        numReportedMeas   = 0;
        samplesThisPeriod = 0;
        totalNumSamples   = 0;
        measPeriod = 1;
    };

#pragma GCC diagnostic ignored "-Wunused-parameter"
    void MeasReady(MeasData *dataPtr)
    {
        numReportedMeas++;
    }

    MeasParams GetMeasParams()
    {
        MeasParams measParams = {
                .rssiParams = {
                     chBitmask : 0x0,
                     measPeriod : 1
        }};
        return measParams;
    };

    void SetMeasPeriod(uint32_t period)
    {
        measPeriod = period;
    }

    uint32_t GetSampleCount()
    {
        return totalNumSamples;
    }

    uint32_t measPeriod;
    uint32_t samplesThisPeriod;
    uint32_t totalNumSamples;
    MeasData measData;
};

#endif
