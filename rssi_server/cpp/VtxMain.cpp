#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/time.h>
#include <ctime>
#include "MeasController.hpp"
#include "LapTimer.hpp"
#include "LapController.hpp"
#include "DataSaver.hpp"
#include "GlobalDefs.hpp"

uint32_t counter = 0;
volatile uint32_t interruptBuffer = 0;
volatile bool stopMeas = false;

class VtxTimer
{
public:
    static MeasController &GetMeasCntrl();

    VtxTimer()
    {
        printf("Starting up VtxTimer\n");
    }
};

MeasController& VtxTimer::GetMeasCntrl()
{
    static MeasController measController = MeasController();
    return measController;
}

static VtxTimer vtxTimer;

void NewMeasurement(int signal)
{
    std::clock_t start, stop;
    start = std::clock();
    interruptBuffer = (interruptBuffer << 1) | 0x1;
    vtxTimer.GetMeasCntrl().TriggerAllMeasurements();
    counter++;
    stop = std::clock();
//    printf("Running time: %f, intBuf: 0x%02X, cntr: %5d\n", (stop - start) / (double)(CLOCKS_PER_SEC / 1000), interruptBuffer, counter);
}

void StopMeasurement(int sigal)
{
    printf("Stopping measurements\n");
    stopMeas = true;
}

void InitVtxTimer(uint32_t samplingTimeUs)
{
    struct itimerval timer;
    timer.it_interval.tv_sec = 0;
    timer.it_interval.tv_usec = samplingTimeUs; // Sampling time
    timer.it_value.tv_sec = 0;
    timer.it_value.tv_usec= 10; // Time until first sample is captured

    signal(SIGALRM, NewMeasurement);
    signal(SIGINT, StopMeasurement);
    
    int retval = setitimer(ITIMER_REAL, &timer, NULL);
    if (retval != 0)
        printf("Failed to set timer: %d\n", retval);
}

int main(void)
{
    MeasParams params = {.rssiParams = { chBitmask : 0, measPeriod : 10}};
    LapController *lapCntrl = new LapController();
    LapTimer      *timerCh1 = new LapTimer(params, *lapCntrl);
    vtxTimer.GetMeasCntrl().RegisterObserver(timerCh1, RssiMeasType);

    InitVtxTimer(lapCntrl->GetSamplingTime());
    while(1)
    {
        sleep(1);
        if (counter > 1000)
        {
            lapCntrl->recalibrate = false;
        }
        if (stopMeas)
        {
            std::vector<RssiMeas> data = timerCh1->GetLastNSamples(100000);
            DataSaver *dSaver = new DataSaver();
            dSaver->SaveToFile(data, data.size(), rssiOutputFile);
            exit(1);
        }
    }
    return 0;
}

