#ifndef DATA_SAVER_HPP
#define DATA_SAVER_HPP
#include "MeasTypes.hpp"
#include <string>

using namespace std;

class DataSaver
{
public:
    template<class Type>
    void SaveToFile(Type data, uint32_t length, string fileName);
private:
};

#endif
