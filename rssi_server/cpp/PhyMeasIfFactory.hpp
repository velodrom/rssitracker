#ifndef PHY_MEAS_IF_FACTORY_HPP
#define PHY_MEAS_IF_FACTORY_HPP

#include "MeasTypes.hpp"
#include "Rx5808.hpp"

class PhyMeasIfFactory
{
public:
    PhyMeasIf *CreatePhyMeasObj(MeasName measName);

private:
    Rx5808 *CreateRx5808Obj();
    Rx5808 *CreateRx5808FileStreamObj();
};

#endif
