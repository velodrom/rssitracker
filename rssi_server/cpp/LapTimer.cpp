#include <cstring>
#include <stdio.h>
#include <algorithm>

#include "LapTimer.hpp"

LapTimer::LapTimer(MeasParams params, LapController &lapCntrl) :
    rawSampleBuffer({0}),
    currentSampleIdx(0),
    newSamplePtr(rawSampleBuffer),
    lapCntrl(lapCntrl),
    maxRssi(0),
    peakThreshold(0.7),
    findFallingEdge(true)
{
    if (params.rssiParams.measPeriod > maxNumMeasurements)
        params.rssiParams.measPeriod = maxNumMeasurements;
    measParams = params;
    firFilter = new FirFilter();
}

void LapTimer::MeasReady(MeasData *data)
{
    SaveSamples(data);
    uint32_t numSamples = data->rssiMeasData.numRssiMeas;
    uint32_t filteredRssi[maxNumMeasurements] = {0};
    firFilter->Filter(&data->rssiMeasData.rssiMeas[0], &filteredRssi[0], data->rssiMeasData.numRssiMeas);

    if (lapCntrl.recalibrate)
        CalibrateMaxSignalLevel(&filteredRssi[0], numSamples);
    else
        DetectPeakEdges(&filteredRssi[0], numSamples);
}

MeasParams LapTimer::GetMeasParams()
{
    return measParams;
}

void LapTimer::SetMeasPeriod(uint32_t period)
{
    if (period < maxNumMeasurements)
        measParams.rssiParams.measPeriod = period;
    else
        measParams.rssiParams.measPeriod = maxNumMeasurements;
}

uint32_t LapTimer::GetSampleCount()
{
    return totalNumSamples;
}

uint32_t LapTimer::CalibrateMaxSignalLevel(uint32_t *rssiData, uint32_t numSamples)
{
//    printf("Calibrating..\n");
    for (uint32_t idx = 0; idx < numSamples; idx++)
    {
        if (rssiData[idx] > maxRssi)
            maxRssi = rssiData[idx];
    }
    return maxRssi;
}

vector<RssiMeas> LapTimer::GetLastNSamples(const uint32_t numSamples)
{
    uint32_t numSamplesToReturn = min(numSamples, totalNumSamples);
    vector<RssiMeas> retSamples;
    retSamples.resize(retSamples.size() + numSamplesToReturn);
    if (totalNumSamples > 0)
    {
        if (numSamplesToReturn <= currentSampleIdx)
        {
            memcpy(&retSamples[0],
                        &rawSampleBuffer[currentSampleIdx-numSamplesToReturn],
                        numSamplesToReturn*sizeof(RssiMeas));
        }
        else
        {
            uint32_t numSamplesFromStart = currentSampleIdx;
            uint32_t numSampelsFromEnd   = numSamplesToReturn - currentSampleIdx;
            // First copy samples from end (older samples)
            memcpy(&retSamples[0],
                        &rawSampleBuffer[maxSampleBufferSize - numSampelsFromEnd],
                        numSampelsFromEnd*sizeof(RssiMeas));
            // Second copy samples from beginning (newer samples)
            memcpy(&retSamples[numSampelsFromEnd],
                        &rawSampleBuffer[0],
                        numSamplesFromStart*sizeof(RssiMeas));
        }
    }

    return retSamples;
}

void LapTimer::SaveSamples(MeasData *data)
{
    uint32_t numNewSamples = data->rssiMeasData.numRssiMeas;
    currentSampleIdx += numNewSamples;
    totalNumSamples  += numNewSamples;
    if (currentSampleIdx <= maxSampleBufferSize)
    {
        memcpy(newSamplePtr, &(data->rssiMeasData.rssiMeas[0]), numNewSamples*sizeof(RssiMeas));
        newSamplePtr += numNewSamples;
    }
    else
    {
        uint32_t numSamplesToBeginning = currentSampleIdx - maxSampleBufferSize;
        uint32_t numSamplesToEnd       = numNewSamples - numSamplesToBeginning;
        if (numSamplesToEnd > 0)
        {
            memcpy(newSamplePtr,
                        &(data->rssiMeasData.rssiMeas[0]),
                        numSamplesToEnd*sizeof(RssiMeas));
        }
        newSamplePtr = &rawSampleBuffer[0];
        memcpy(newSamplePtr,
                    &(data->rssiMeasData.rssiMeas[numSamplesToEnd]),
                    numSamplesToBeginning*sizeof(RssiMeas));
        newSamplePtr    += numSamplesToBeginning;
        currentSampleIdx = numSamplesToBeginning;
    }
}

void LapTimer::SaveSamples(RssiMeas *data, uint32_t numNewSamples)
{
    currentSampleIdx += numNewSamples;
    totalNumSamples  += numNewSamples;
    if (currentSampleIdx <= maxSampleBufferSize)
    {
        memcpy(newSamplePtr, &(data[0]), numNewSamples*sizeof(RssiMeas));
        newSamplePtr += numNewSamples;
    }
    else
    {
        uint32_t numSamplesToBeginning = currentSampleIdx - maxSampleBufferSize;
        uint32_t numSamplesToEnd       = numNewSamples - numSamplesToBeginning;
        if (numSamplesToEnd > 0)
        {
            memcpy(newSamplePtr,
                        &(data[0]),
                        numSamplesToEnd*sizeof(RssiMeas));
        }
        newSamplePtr = &rawSampleBuffer[0];
        memcpy(newSamplePtr,
                    &(data[numSamplesToEnd]),
                    numSamplesToBeginning*sizeof(RssiMeas));
        newSamplePtr    += numSamplesToBeginning;
        currentSampleIdx = numSamplesToBeginning;
    }
}

void LapTimer::DetectPeakEdges(uint32_t *rssiData, uint32_t numSamples)
{
    uint32_t threshold    = (uint32_t)maxRssi*peakThreshold;
    uint32_t samplingFreq = lapCntrl.GetSamplingFreq();
    if (findFallingEdge)
    {
        uint32_t lastPeakIdx = peakFallingEdgeIdx.size() > 0 ? peakFallingEdgeIdx.back() : 0;
        for (uint32_t idx = 0; idx < numSamples; idx++)
        {
            printf("Finding FE %d < %d (fs: %d) and %d (%d,lp %d) > %d\n",
                    rssiData[idx],threshold,samplingFreq,
                    (totalNumSamples - numSamples + idx) - lastPeakIdx,
                    totalNumSamples,
                    lastPeakIdx,
                    lapCntrl.minPeakFallingEdgeDist*samplingFreq);
            if (rssiData[idx] < threshold &&
                    ((totalNumSamples - numSamples + idx) - lastPeakIdx > (lapCntrl.minPeakFallingEdgeDist*samplingFreq)))
            {
                printf("Found FALLING edge at %d: %d\n", (totalNumSamples - numSamples + idx), rssiData[idx]);
                peakFallingEdgeIdx.push_back((totalNumSamples - numSamples + idx));
                findFallingEdge = false;
                break;
            }
        }
    }
    else // Find Rising edge
    {
        uint32_t lastPeakIdx = peakFallingEdgeIdx.back();
        for (uint32_t idx = 0; idx < numSamples; idx++)
        {
            printf("Finding RE %d > %d (fs: %d) and %d (%d,lp %d) > %d\n",
                    rssiData[idx],threshold,samplingFreq,
                    (totalNumSamples - numSamples + idx) - lastPeakIdx,
                    totalNumSamples,
                    lastPeakIdx,
                    lapCntrl.minPeakFallingEdgeDist*samplingFreq);
            if (rssiData[idx] > threshold &&
                    ((totalNumSamples - numSamples + idx) - lastPeakIdx > (lapCntrl.minPeakRisingEdgeDist*samplingFreq)))
            {
                printf("Found RISING edge at %d: %d\n", (totalNumSamples - numSamples + idx), rssiData[idx]);
                peakRisingEdgeIdx.push_back((totalNumSamples - numSamples + idx));
                findFallingEdge = true;
                break;
            }
        }
    }
}

