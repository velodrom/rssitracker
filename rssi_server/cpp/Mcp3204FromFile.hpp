#ifndef MCP_3204_FROM_FILE_HPP
#define MCP_3204_FROM_FILE_HPP
#include "AdcIf.hpp"
#include <stdio.h>
#include <string>

using namespace std;

class Mcp3204FromFile : public AdcIf
{
public:
    Mcp3204FromFile(string fileNameIn);
    ~Mcp3204FromFile();
    bool Configure();
    bool IsConfigured();
    uint32_t Measure();

private:
    string   fileName;
    FILE*    pFile;
};

#endif
