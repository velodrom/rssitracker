#include "DataSaver.hpp"
#include <stdio.h>
#include <vector>

template<>
void DataSaver::SaveToFile(uint32_t *data, uint32_t length, string fileName)
{
    FILE *pFile;
    pFile = fopen(fileName.c_str(), "w");
    if (pFile != NULL)
    {
        for (uint32_t idx = 0; idx < length; idx++)
            fprintf(pFile, "rssi: %d\n", data[idx]);
    }
    fclose(pFile);
}

template<>
void DataSaver::SaveToFile(vector<RssiMeas> data, uint32_t length, string fileName)
{
    FILE *pFile;
    pFile = fopen(fileName.c_str(), "w");
    if (pFile != NULL)
    {
        for (uint32_t idx = 0; idx < length; idx++)
            fprintf(pFile, "rssi: %d\n", data[idx]);
    }
    fclose(pFile);
}
