#ifndef MEAS_CONTROLLER_HPP
#define MEAS_CONTROLLER_HPP

#include <vector>
#include <map>
#include "MeasObserver.hpp"
#include "PhyMeasIf.hpp"
#include "PhyMeasIfFactory.hpp"

typedef struct PhyMeasObserverPair
{
    PhyMeasIf    *phyMeasIf;
    MeasObserver *measObserver;
} PhyMeasObserverPair;

typedef std::vector<PhyMeasObserverPair*> PhyObserverVector;
typedef std::map<MeasName, PhyObserverVector*> PhyMeasMap;

class MeasController
{
public:
    MeasController();

    bool RegisterObserver(MeasObserver *observer, MeasName measName);
    void UnRegisterObserver(MeasObserver *observer, MeasName measName);
    void TriggerAllMeasurements();
    bool MeasurementExists(MeasName measName);

//protected:
    uint32_t NumRegObservers(MeasName measName);
    uint32_t NumMeasurements();
private:
    void TriggerMeasurementsFor(PhyObserverVector* phyMeasObsVect);

    PhyMeasMap        phyMeasMap;
    PhyMeasIfFactory  phyMeasFactory;
};

#endif
