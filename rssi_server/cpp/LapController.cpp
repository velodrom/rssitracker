#include "LapController.hpp"

LapController::LapController() :
    recalibrate(true),
    minPeakRisingEdgeDist(4), // in seconds
    minPeakFallingEdgeDist(1), // in seconds
    samplingTimeUs(10000)
{
}

void LapController::SetSamplingTime(uint32_t tsUs)
{

    if (tsUs < maxSamplingTimeUs)
        samplingTimeUs = tsUs;
}

uint32_t LapController::GetSamplingTime()
{
    return samplingTimeUs;
}

uint32_t LapController::GetSamplingFreq()
{
    return (uint32_t)(1000000.0/((float)samplingTimeUs));
}
