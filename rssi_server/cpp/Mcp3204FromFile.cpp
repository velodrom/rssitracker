#include "Mcp3204FromFile.hpp"

Mcp3204FromFile::Mcp3204FromFile(string fileNameIn)
{
    fileName = fileNameIn;
    pFile = fopen(fileName.c_str(), "r");
}

Mcp3204FromFile::~Mcp3204FromFile()
{
    fclose(pFile);
}

bool Mcp3204FromFile::Configure()
{
    if (pFile != NULL)
        return true;
    else
        return false;
}

bool Mcp3204FromFile::IsConfigured()
{
    return Configure();
}

uint32_t Mcp3204FromFile::Measure()
{
    uint32_t retRssi = 0;
    if (pFile != NULL)
    {
        fscanf(pFile, "rssi: %d\n", &retRssi);
        if (retRssi == 0)
        { // Seek to beginning of file to loop data
            fseek(pFile, 0, SEEK_SET);
            fscanf(pFile, "rssi: %d\n", &retRssi);
        }
    }
    return retRssi;
}
