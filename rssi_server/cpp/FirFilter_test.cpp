#include <gtest/gtest.h>
#include "FirFilter.hpp"

using namespace ::testing;

class FirFilterTest : public Test
{
public:
    FirFilterTest()
    {
        firFilter = new FirFilter();
    }
protected:
    FirFilter *firFilter;
};

TEST_F(FirFilterTest, ImpulseInput)
{
    const uint32_t inputLen      = 6;
    uint32_t input[inputLen]     = {32767,0,0,0,0,0}; // ~1 in Q16.15 format
    uint32_t output[inputLen]    = {0};
    uint32_t refOutput[inputLen] = {1385, 6337, 10925, 10925, 6337, 1385};

    firFilter->Filter(&input[0], &output[0], inputLen);

    for (uint32_t idx = 0; idx < inputLen; idx++)
        ASSERT_EQ(refOutput[idx], output[idx]);
}

TEST_F(FirFilterTest, RandomInputOfLength10)
{
    const uint32_t inputLen      = 10;
    uint32_t input[inputLen]     = {41, 249, 246, 125, 205, 37, 108, 235, 203, 246};
    uint32_t output[inputLen]    = {0};
    uint32_t refOutput[inputLen] = {1, 18, 72, 149, 205,
                                    214, 179, 146, 147, 179};

    firFilter->Filter(&input[0], &output[0], inputLen);

    for (uint32_t idx = 0; idx < inputLen; idx++)
        ASSERT_EQ(refOutput[idx], output[idx]);
}

TEST_F(FirFilterTest, FilterStateIsPersistentOverConsecutiveCalls)
{
    const uint32_t inputLen      = 12;
    uint32_t input[inputLen]     = {168, 10, 218, 240, 174, 194, 191, 101, 168, 44, 181, 9};
    uint32_t output[inputLen]    = {0};
    uint32_t refOutput[inputLen] = {7, 32, 67, 111, 162, 203, 226, 219, 198, 176, 151, 133};

    firFilter->Filter(&input[0], &output[0], inputLen/2);
    firFilter->Filter(&input[6], &output[6], inputLen/2);

    for (uint32_t idx = 0; idx < inputLen; idx++)
        ASSERT_EQ(refOutput[idx], output[idx]);
}
