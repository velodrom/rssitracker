#ifndef MEAS_OBSERVER_HPP
#define MEAS_OBSERVER_HPP

#include "MeasTypes.hpp"

class MeasObserver
{
public:
    virtual void       MeasReady(MeasData *dataPtr) = 0;
    virtual MeasParams GetMeasParams() = 0;
    virtual void       SetMeasPeriod(uint32_t period) = 0;
    virtual uint32_t   GetSampleCount() = 0;

    uint32_t   numReportedMeas;
    uint32_t   observerId; // Identifier to distinguish between different observers
};

#endif
