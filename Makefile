# A sample Makefile for building Google Test and using it in user
# tests.  Please tweak it to suit your environment and project.  You
# may want to move it to your project's root directory.
#
# SYNOPSIS:
#
#   make [all]  - makes everything.
#   make TARGET - makes the given target.
#   make clean  - removes all files generated by make.

# Please tweak the following variable definitions as needed by your
# project, except GTEST_HEADERS, which you can use in your own targets
# but shouldn't modify.

# Points to the root of Google Test, relative to where this file is.
# Remember to tweak this if you move this file.
GTEST_DIR = ../gtest/googletest

# Points to the root of Google Mock, relative to where this file is.
# Remember to tweak this if you move this file.
GMOCK_DIR = ../gtest/googlemock

# Where to find user code.
USER_DIR = .
LIB_DIR  = $(USER_DIR)/hw_drivers/libs

VPATH  = $(USER_DIR)/hw_drivers/rx5808/cpp/
VPATH += $(USER_DIR)/hw_drivers/mcp3204/cpp/
VPATH += $(USER_DIR)/rssi_server/cpp/
#VPATH = /home/vlo/Public/Code/rxserver/hw_drivers/rx5808/cpp/
CUSTOM_HEADERS = /home/vlo/Downloads/wiringpi/wiringPi
CUSTOM_HEADERS += ${USER_DIR}/hw_drivers/mcp3204/cpp
CUSTOM_HEADERS += ${USER_DIR}/hw_drivers/rx5808/cpp
CUSTOM_HEADERS += ${USER_DIR}/rssi_server/cpp

INC_HEADERS=$(foreach d, $(CUSTOM_HEADERS), -I$d)

# Flags passed to the preprocessor.
# Set Google Test's header directory as a system directory, such that
# the compiler doesn't generate warnings in Google Test headers.
CPPFLAGS += -isystem $(GTEST_DIR)/include -isystem $(GMOCK_DIR)/include

# Flags passed to the C++ compiler.
CXXFLAGS += -g -Wall -Wextra -pthread -lwiringPi -std=c++11 -Ofast -funroll-loops

# All tests produced by this Makefile.  Remember to add new tests you
# created to the list.
TESTS = all_unittests

# All Google Test headers.  Usually you shouldn't change this
# definition.
GTEST_HEADERS = $(GTEST_DIR)/include/gtest/*.h \
                $(GTEST_DIR)/include/gtest/internal/*.h

# All Google Mock headers. Note that all Google Test headers are
# included here too, as they are #included by Google Mock headers.
# Usually you shouldn't change this definition.	
GMOCK_HEADERS = $(GMOCK_DIR)/include/gmock/*.h \
                $(GMOCK_DIR)/include/gmock/internal/*.h \
                $(GTEST_HEADERS)

# House-keeping build targets.

all : $(TESTS)

clean :
	rm -f $(TESTS) *.o

clean_all :
	rm -f $(TESTS) gtest.a gtest_main.a gmock_main.a *.o

# Builds gtest.a and gtest_main.a.

# Usually you shouldn't tweak such internal variables, indicated by a
# trailing _.
GTEST_SRCS_ = $(GTEST_DIR)/src/*.cc $(GTEST_DIR)/src/*.h $(GTEST_HEADERS)
GMOCK_SRCS_ = $(GMOCK_DIR)/src/*.cc $(GMOCK_HEADERS)

# For simplicity and to avoid depending on Google Test's
# implementation details, the dependencies specified below are
# conservative and not optimized.  This is fine as Google Test
# compiles fast and for ordinary users its source rarely changes.
gtest-all.o : $(GTEST_SRCS_)
	$(CXX) $(CPPFLAGS) -I$(GTEST_DIR) -I$(GMOCK_DIR) $(CXXFLAGS) \
            -c $(GTEST_DIR)/src/gtest-all.cc

gmock-all.o : $(GMOCK_SRCS_)
	$(CXX) $(CPPFLAGS) -I$(GTEST_DIR) -I$(GMOCK_DIR) $(CXXFLAGS) \
            -c $(GMOCK_DIR)/src/gmock-all.cc

gmock_main.o : $(GMOCK_SRCS_)
	$(CXX) $(CPPFLAGS) -I$(GTEST_DIR) -I$(GMOCK_DIR) $(CXXFLAGS) \
            -c $(GMOCK_DIR)/src/gmock_main.cc

gmock.a : gmock-all.o gtest-all.o
	$(AR) $(ARFLAGS) $@ $^

gmock_main.a : gmock-all.o gtest-all.o gmock_main.o
	$(AR) $(ARFLAGS) $@ $^

# Builds a sample test.  A test should link with either gtest.a or
# gtest_main.a, depending on whether it defines its own main()
# function.

APPSRCS  = Rx5808.cpp
APPSRCS += MeasController.cpp
APPSRCS += LapController.cpp
APPSRCS += RxWrapper.cpp
APPSRCS += Mcp3204.cpp
APPSRCS += Mcp3204FromFile.cpp
APPSRCS += LapTimer.cpp
APPSRCS += PhyMeasIfFactory.cpp
APPSRCS += FirFilter.cpp
APPSRCS += DataSaver.cpp

TSTSRCS += MeasController_test.cpp
TSTSRCS += LapController_test.cpp
TSTSRCS += Rx5808_test.cpp
TSTSRCS += Mcp3204_test.cpp
TSTSRCS += Mcp3204FromFile_test.cpp
TSTSRCS += LapTimer_test.cpp
TSTSRCS += PhyMeasIfFactory_test.cpp
TSTSRCS += FirFilter_test.cpp

APPOBJS = $(APPSRCS:.cpp=.o)
TSTOBJS = $(TSTSRCS:.cpp=.o)

vtxmain : $(APPOBJS) VtxMain.o
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -lpthread $^ -o $@
	
all_unittests : $(APPOBJS) $(TSTOBJS) gmock_main.a
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -lpthread $^ -o $@

.cpp.o: $(GMOCK_HEADERS)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) ${INC_HEADERS} -c -fPIC $< -o $@

runut : all_unittests
	./all_unittests

librx: RxWrapper.o Rx5808.o Mcp3204.o
	$(CXX) -shared -Wl,-soname,librx.so -o $(LIB_DIR)/librx.so RxWrapper.o Rx5808.o Mcp3204.o $(LIB_DIR)/libwiringPi.so